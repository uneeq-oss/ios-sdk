//
//  ViewController.swift
//  QuickStart-Example
//

import UIKit
import DigitalHumanSDK
import AVFoundation

class ViewController: UIViewController {
    
    @IBOutlet weak var remoteView: UIView!
    @IBOutlet weak var localView: UIView!
    @IBOutlet weak var talkButton: UIButton!
    
    lazy var digitalHumanSDK: DigitalHumanSDK = {
        let options = DHOptions(workspaceId: "A-WORKSPACE-ID",
                                clientSecret: "A-CLIENT-SECRET",
                                url: "https://syd-eeva.faceme.com")
        
        let digitalHumanSDK = DigitalHumanSDK(options: options)
        
        digitalHumanSDK.loggingLevel = .verbose
        
        return digitalHumanSDK
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.talkButton.backgroundColor = .blue
        
        self.checkPermissions()
    }
 
    func checkPermissions() {
        // Microphone permission
        if AVAudioSession.sharedInstance().recordPermission == .undetermined {
            // Request permission
            AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                // Handle granted
            })
        }
        
        // Camera permission
        if AVCaptureDevice.authorizationStatus(for: .video) == .notDetermined {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: {accessGranted in
            })
        }
    }

    // MARK: - Actions
    @IBAction func start(_ sender: Any) {
        self.digitalHumanSDK.messageDelegate = self
        self.digitalHumanSDK.avatarDelegate = self
        self.digitalHumanSDK.startSession(sessionId: "A-SESSION-ID")
    }
    
    @IBAction func stop(_ sender: Any) {
        self.digitalHumanSDK.endSession()
    }
    
    @IBAction func talkDown(_ sender: Any) {
        self.talkButton.backgroundColor = .red
        
        self.digitalHumanSDK.startRecording()
    }
    
    @IBAction func talkUp(_ sender: Any) {
        self.talkButton.backgroundColor = .blue
        
        self.digitalHumanSDK.stopRecording()
    }
}

extension ViewController: DHTextDelegate {
    func digitalHumanTextReceived(_ text: String) {
        print(text)
    }
}

extension ViewController: DHMessageDelegate {
    
    func digitalHumanMessage(_ message: DHMessage) {
        print("digitalHumanMessage received: \(message)")
        
        switch message {
            case .sessionReady:
                // Get the remote video view for the Digital Human from the SDK
                if let view = self.digitalHumanSDK.getRemoteVideoView(frame: self.remoteView.frame) {
                    self.remoteView.addSubview(view)
                    view.translatesAutoresizingMaskIntoConstraints = false
                    
                    NSLayoutConstraint.activate([
                        view.leadingAnchor.constraint(equalTo: self.remoteView.leadingAnchor),
                        view.trailingAnchor.constraint(equalTo: self.remoteView.trailingAnchor),
                        view.topAnchor.constraint(equalTo: self.remoteView.topAnchor),
                        view.bottomAnchor.constraint(equalTo: self.remoteView.bottomAnchor)
                        ])
                }
                
                // Get the local video view for the Digital Human from the SDK
                if let view = self.digitalHumanSDK.getLocalVideoPreviewView(frame: self.localView.frame) {
                    self.localView.addSubview(view)
                    view.translatesAutoresizingMaskIntoConstraints = false
                    
                    NSLayoutConstraint.activate([
                        view.leadingAnchor.constraint(equalTo: self.localView.leadingAnchor),
                        view.trailingAnchor.constraint(equalTo: self.localView.trailingAnchor),
                        view.topAnchor.constraint(equalTo: self.localView.topAnchor),
                        view.bottomAnchor.constraint(equalTo: self.localView.bottomAnchor)
                        ])
                }

            
            case .sessionEnded:
                for subview in self.remoteView.subviews {
                    subview.removeFromSuperview()
                }
                
                for subview in self.localView.subviews {
                    subview.removeFromSuperview()
                }
            default:
                break
        }
        
    }
}
